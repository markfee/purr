const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
    template: "./src/app/layouts/index.html",
    filename: "./index.html",
    favicon: './src/styles/favicon.ico',
    port: 8080
});

module.exports = {
    entry: {
        purr: './src/purr.js'
    },
    output: {
        publicPath: '/'      
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            resolve: { 
                extensions: [".js", ".jsx"] 
            },
            use: { loader: "babel-loader" }
        }, {
            test: /\.styl$/,
            resolve: { 
                extensions: [".styl"] 
            },
            use: [
              { loader: "style-loader" }, // creates style nodes from JS strings
              { loader: "css-loader" },   // translates CSS into CommonJS
              { loader: "stylus-loader" } // compiles Stylus to CSS
            ]
        }]
    },
    plugins: [htmlPlugin]
};