export const apiActionTypes = Object.freeze({
    DELETE: "DELETE", GET: "GET", PATCH: "PATCH", POST: "POST", PUT: "PUT"
});

export const invalidate = (resource, id) => (
    {type: 'INVALIDATE', resource: resource, id: id});
export const get = (resource, id, force) => (
    {type: 'GET', resource, id, force});
export const save = (resource, values, id) => (
    {type: 'POST', resource: resource, values: values, id});
export const importStatement = (accountId, filepaths) => (
    {type: 'POST', resource: "accounts", id: `${accountId}/statements`, values: {filepaths}});
export const destroy = (resource, id) => (
    {type: 'DELETE', resource: resource, id: id});
export const received = (resource, data, id) => ({
    type: 'RECEIVED', resource, data, id});
export const deleted = (resource, id) => ({
    type: 'DELETED', resource, id});
export const edit = (resource, id, editing=true) => (
    {type: 'EDITING', resource, id, editing});

export const apiActions = {invalidate, get, save, destroy, received, deleted, edit};
export default apiActions;