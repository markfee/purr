const request = require('superagent');
const path = require('path');
import {apiActions, apiActionTypes} from './api-actions';
import {STALE} from './make-api-reducer';

const handleResponse = (action, response, next) => {
    if (action.resource === "browser") {
        next(apiActions.received(action.resource, response.body, action.id ));
    } else {
        const items = {};
        [].concat(response.body).forEach((item) => (items[item.id] = item));
        next(apiActions.received(action.resource, items, action.id));
    }
};

const api = store => next => action => {
    if (!apiActionTypes[action.type]) {
         return next(action); 
    }

    const resourcePath = path.join('/api', action.resource, action.id || "");
    const parameters = action.parameters || {};
    switch (action.type) {
        case apiActionTypes.GET: {
            if (action.id || store.getState()[action.resource] === STALE || action.force) {
                request
                    .get(resourcePath)
                    .set('Accept', 'application/json')
                    .query(parameters)
                    .then((response) => handleResponse(action, response, next));
            }
            break;
        }
        case apiActionTypes.POST: {
            request
                .post(resourcePath)
                .set('Accept', 'application/json')
                .send(action.values)
                .then((response) => handleResponse(action, response, next));
            break;
        }
        case apiActionTypes.DELETE: {
            request
                .delete(resourcePath)
                .set('Accept', 'application/json')
                .then((response) =>
                    next(apiActions.deleted(action.resource, action.id)));
            break;
        }        
    }
    return next(action);
};

export default api;