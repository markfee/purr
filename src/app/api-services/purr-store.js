import { createStore, combineReducers, applyMiddleware } from 'redux'
import {account, accountTransformer} from '../accounts/account-reducer';
import makeApiReducer from './make-api-reducer'
import browser from '../import/browser-reducer'
import api from './api-data-service'

export default createStore(combineReducers({
    account: account,
    accounts: makeApiReducer('accounts'), 
    browser
}), applyMiddleware(api, accountTransformer));