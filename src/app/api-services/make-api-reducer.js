export const STALE = Object.freeze({});

const makeApiReducer = (resource) => (state = STALE, action) => {
    if (action.resource !== resource) {
        return state;
    }
    switch (action.type) {
        case 'INVALIDATE': return STALE;
        case 'EDITING': {
            const editing = Object.assign({}, state[action.id], {editing: action.editing});
            return Object.assign({}, state, {[action.id]: editing});
        }
        case 'RECEIVED': {
            return Object.assign({}, state, action.data);
        }
        case 'DELETED': {
            if (state[action.id]) {
                const newState = Object.assign({}, state);
                delete newState[action.id];
                return newState;
            }
            return state;
        }
    }
    return state;
};

export default makeApiReducer;