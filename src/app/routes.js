import React from "react";
import {connect} from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {get} from './api-services/api-actions'
import Account from './accounts/account';
import Accounts from './accounts/accounts';
import Browser from './import/browser';
import ImportTransactions from './import/import-transactions';
import MainLayout from './layouts/main-layout'

const Home = () => <MainLayout path="Home"><div>Home Page</div></MainLayout>;
const Transactions = () => <MainLayout path="Home"><div>Transactions</div></MainLayout>;

export default connect((purrStore) => ({
    accounts: purrStore.accounts
}), {get})(
class Routes extends React.Component {

    componentDidMount() {
        this.props.get('accounts');
    }
    
    componentDidUpdate() {
    }

    render() {
        return <Router>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/accounts/:id' component={Account} />
                <Route path='/accounts' component={Accounts} />
                <Route path='/import/browser' component={Browser} />
                <Route path='/import/transactions' component={ImportTransactions} />
                <Route path='/transactions' component={Transactions} />
                <Route component={Home} />
            </Switch>
        </Router>;
    }
});