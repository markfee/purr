import React from "react";
import {ValuePair, AmountValue, DateValue} from "../core/components";

export const Transaction = (props) => 
    <div className={props.className}>
        <div className='values'>
            <h2>{props.transaction.description}</h2>
            <DateValue label="Date" value={props.transaction.date} />
            <AmountValue label="Amount">{props.transaction.amount}</AmountValue>
            <AmountValue label="Balance">{props.transaction.balance}</AmountValue>
        </div>    
    </div>

export const Transactions = (props) => 
    (props.values || props.children || []).map((transaction, index) => 
        <Transaction 
            className='transaction'
            key={index} 
            transaction={transaction} />);
