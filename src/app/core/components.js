import React from "react";
import moment from "moment";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
const Amount = require("../../api/amounts/amount");

export const TextInput = (props) => <React.Fragment>
    <label>{props.label}:</label> 
    <input type='text' value={props.value} onChange={props.onChange} name={props.name}/>
</React.Fragment>

export const ValuePair = (props) => <React.Fragment>
    <label>{props.label}:</label> 
    <span>{props.value || props.children}</span>
</React.Fragment>

export const IconButton = (props) => <React.Fragment>
    <button disabled={props.disabled} onClick={props.onClick}>
        <FontAwesomeIcon icon={props.icon} />
    </button>
</React.Fragment>

export const AmountValue = (props) => {
    const amount = new Amount((props.value === undefined ? props.children : props.value) || 0);
    return <React.Fragment>
        {props.label && <label>{props.label}:</label>}
        <span className={'currency' + (amount < 0 && ' negative' || '')}>
            {amount.toString()}
        </span>
    </React.Fragment>
}

export const DateValue = (props) => <React.Fragment>
    {props.label && <label>{props.label}:</label>}
    <span className='date'>{
        moment(props.value).format(props.format || 'DD/MM/YYYY')
    }</span>
</React.Fragment>

export const MinDateValue = (props) => <DateValue {...props} value={moment.min(props.dates.map(date => moment(date)))} />

export const ValuePicker = (props) => <React.Fragment>
        {props.label && <label>{props.label}</label>} 
        <select onChange={(e) => props.onChange(e.target.value)} value={props.value}> {
            props.values.map((value) => 
                <option key={value.id} value={value.id}>
                    {value.name}
                </option>)
        }</select>
    </React.Fragment>