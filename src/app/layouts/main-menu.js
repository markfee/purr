import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Link } from 'react-router-dom';

const MainMenu = (props) => {
    return <div className='main-menu'>
        <ul>
            <li><Link to='/'>Home</Link></li>
            <li><Link to='/accounts'>Accounts</Link></li>
            <li><Link to='/import/browser'>Import</Link></li>
            <li><Link to='/transactions'>Transactions</Link></li>
            <li>
                API
                <ul>
                    <li><a href='/api/accounts'>accounts</a></li>
                    <li><a href='/api/browser'>browser</a></li>
                </ul>
            </li>
        </ul>
    </div>
};

export {MainMenu}