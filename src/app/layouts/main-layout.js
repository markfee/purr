import React from "react";
import {MainMenu} from './main-menu';

const Header = (props) => 
    <div className='header'>
        <h1>Purr >> {props.path}</h1>
    </div>;

const Footer = () => 
    <div className='footer'>
        <h1>Purr</h1>
    </div>;

class MainLayout extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const className = this.props.path.toLowerCase();
        return <div>
            <Header path={this.props.path} />
            <MainMenu path={this.props.path} />
            <div className={`main-content ${className}`}>
                {this.props.children}
            </div>
            <Footer />
        </div>
    }
};

export default MainLayout;