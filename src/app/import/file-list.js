import React from "react";
import { Link } from 'react-router-dom';

const path = require('path');

export default class FileList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (!this.props.contents && this.props.contents.map) {
            return null
        }
        const showCheckbox = this.props.checked && this.props.onCheck && true;
        const links = this.props.contents.map((file, index) => 
            <li key={index}>
                {showCheckbox && <input type='checkbox' 
                    name={file} 
                    checked={this.props.checked[file] || false}
                    onChange={(e) => this.props.onCheck(file, e.target.checked)}
                />}
                <Link to={path.join(this.props.rootPath, file)}>
                    {file}
                </Link> 
            </li>) || null;

        return <div>
            <ul>{links}</ul>
        </div>;
    }
}