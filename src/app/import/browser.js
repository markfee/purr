import React from "react";
import ReactDOM from "react-dom";
import {connect} from 'react-redux'
import MainLayout from '../layouts/main-layout'
import {get} from '../api-services/api-actions'
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import ImportTransactions from './import-transactions';
import FileList from './file-list';

const path = require('path');

export default connect((purrStore) => ({
    browser: purrStore.browser
}), {get})(
class Browser extends React.Component {
    constructor(props) {
        super(props);
        this.firstRender = true;
    }

    updateBrowser() {
        // if filePath doesn't match browser path then update the browser
        const filePath = this.browserFilePath();
        if (filePath !== this.props.browser.path) {
            this.props.get('browser', filePath);
        }
    }

    browserFilePath() {
        return path.join(this.props.location.pathname.replace(this.props.match.url, ''));
    }


    componentDidMount() {
        this.unlisten = this.props.history.listen((location, action) => {
            this.updateBrowser();
        });
        this.updateBrowser();
    }

    componentDidUpdate(prevProps) {
        this.updateBrowser();
    }

    componentWillUnmount() {
        this.unlisten();
    }

    render() {
        const browser = this.props.browser;

        if (browser.content && browser.content.transactions) {
            if (this.firstRender) {
                return null;
            }
            return <Redirect to={path.join("/import/transactions", browser.path)} />;
        }
        this.firstRender = false;

        const rootPath = path.join('/import/browser/', this.browserFilePath());
        return <MainLayout path={`Browser /${this.props.browser.path}`}>
                <div className='browser'>
                    <h2>Browser</h2>
                    {<FileList contents={this.props.browser.content} rootPath={rootPath} />}
                </div>
            </MainLayout>;
    }
})