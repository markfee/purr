import React from "react";
import ReactDOM from "react-dom";
import { connect } from 'react-redux'
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import MainLayout from '../layouts/main-layout'
import { Transactions } from '../transactions/transaction';
import { IconButton, ValuePair, AmountValue, DateValue, ValuePicker } from '../core/components';
import { get, importStatement } from '../api-services/api-actions';

const path = require('path');

export default connect((purrStore) => ({
    browser: purrStore.browser,
    accounts: purrStore.accounts
}),{get, importStatement} )(
class ImportTransactions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            account: {},
            redirect: null
        }
        this.importStatement = () => 
            this.props.importStatement(this.state.account.id, this.props.browser.path);
    }

    initialiseAccount() {
        if (!this.state.account.id) {
            // accountToSet will match the first account with a matching importFolder
            // or it will default to the first account in  the list
            const filePath = path.dirname(this.browserFilePath());
            const accountIds = Object.keys(this.props.accounts);
            const accountToSet = accountIds.filter(
                (id) => this.props.accounts[id].importFolder === filePath)[0] || accountIds[0];
            const account = this.props.accounts[accountToSet];

            if (account) {
                this.setState({account});
                return true;
            }
        }
        return false;
    }

    browserFilePath() {
        return path.join(this.props.location.pathname.replace(this.props.match.url, ''));
    }
    
    componentDidMount() {
        this.initialiseAccount();
        // if filePath doesn't match the browser path then update the browser
        const filePath = this.browserFilePath();
        if (filePath !== this.props.browser.path) {
            this.props.get('browser', filePath || "");
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // If the props' accounts have updated and a curent account is not set, then 
        // initialiseAccount() wil set this components state to the first available account
        if (this.initialiseAccount()) {
            return;
        }

        // If setState has changed the currentAccount, then there is nothing more to do here.
        const accountHasChanged = this.state.account !== prevState.account;
        if (accountHasChanged) {
            return;
        }

        // Otherwise, check to see if the current account's statements have changed.
        // This indicates that a statement import was successful.
        // If this is the case, then redirect back to the parent folder
        // as the file path for this statement will have moved, invalidating this page.
        const updatedAccount = this.props.accounts[this.state.account.id] || {};
        const updatedStatements = updatedAccount.statements;
        const oldStatements = prevState.account && prevState.account.statements;
        if (updatedStatements !== oldStatements) {
            const parentFolder = path.dirname(this.props.browser.path);
            const newPath = path.join("/import/browser", parentFolder);
            // Refresh the browser with the parent folder 
            this.props.get('browser', parentFolder);
            // and setState to redirect to the new path
            this.setState({redirect: newPath});
        }
    }    

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} path={this.state.redirect}/>;
        }
        const browser = this.props.browser;
        const statement = browser.content  || {};
        const header = statement.header || {};
        const transactions = statement.transactions || [];

        return <MainLayout path={`Import Transactions`}>
            <div className='import-transactions'>
                <h2>Import Transactions</h2>
                <h2>{this.props.browser.path}</h2>

                <div className='form-layout'>
                    <div className='values'>
                        <h2>Statement</h2>
                        <AmountValue label="Brought Forward">{header.broughtForward}</AmountValue>
                        <AmountValue label="Carried Forward">{header.carriedForward}</AmountValue>
                        <DateValue label="Start Date" value={header.startDate} />
                        <DateValue label="End Date" value={header.endDate} />

                        <h2>Import To: {(!this.state.account.id) && 'Please Pick an Account'}</h2>
                        <ValuePicker
                            value={this.state.account.id}
                            values={Object.keys(this.props.accounts)
                                .map((account) => this.props.accounts[account])}
                            label="Account" 
                            onChange={(accountId) => 
                                this.setState({account: this.props.accounts[accountId]})}/>
                        <div className='controls'>
                            <IconButton 
                                onClick={this.importStatement} 
                                icon="file-import" 
                                disabled={!this.state.account}/>
                        </div>
                    </div>
                </div>
                <h2>Transactions</h2>
                <Transactions values={transactions}/>
            </div>
        </MainLayout>;
    }
})