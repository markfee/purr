const defaultState = {path: "", content: []};

const browser = (state = defaultState, action) => {
    if (action.resource !== 'browser') {
        return state;
    }
    switch (action.type) {
        case 'RECEIVED': {
            const newState = Object.assign({}, {path: action.id || "", content: action.data});
            return newState;
        }
        case 'INVALIDATE': {
            return defaultState;
        }        
    }
    return state;
};

export default browser;