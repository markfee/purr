const SET_CURRENT_ACCOUNT = 'SET_CURRENT_ACCOUNT';
const NEW_ACCOUNT = 'NEW_ACCOUNT';
const Account = require('../../api/accounts/account');

export const setCurrentAccount = (accountId) => ({type: SET_CURRENT_ACCOUNT, accountId})
export const createNewAccount = () => ({type: NEW_ACCOUNT})

export const accountTransformer = store => next => action => {
    
    if (action.type === SET_CURRENT_ACCOUNT) {
        const account = store.getState().accounts[action.accountId] || {id: action.accountId};
        if (account) {
            action.account = account;
            next(action);
        }
        return;
    }

    if (action.type === 'RECEIVED' && action.resource === 'accounts') {
        const accountId = store.getState().account.id;
        const account = accountId && action.data[accountId];
        if (account) {
            next({type: SET_CURRENT_ACCOUNT, account});
        }
    }
    next(action)
}

export const account = (state = {}, action) => {
    if (action.type === NEW_ACCOUNT) {
        return new Account();
    }

    if (action.type === SET_CURRENT_ACCOUNT) {
        return new Account(action.account);
    }

    return state;
}