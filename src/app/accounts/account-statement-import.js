import React from "react";
import ReactDOM from "react-dom";
import {connect} from 'react-redux'
import { get, importStatement } from '../api-services/api-actions';
import FileList from '../import/file-list';
import MainLayout from '../layouts/main-layout'
import AccountSummary from './account-summary'
import { IconButton } from '../core/components';
const path = require('path');

export default connect((purrStore) => ({
    account: purrStore.account,
    browser: purrStore.browser,
    filelist: purrStore.browser.content
}), {get, importStatement})(
class Account extends React.Component {
    constructor(props) {
        super(props);
        this.onCheckFile = this.onCheckFile.bind(this);
        this.checkAll = this.checkAll.bind(this);
        this.importStatements = this.importStatements.bind(this);
        this.state = {pathsToImport: {}};
    }

    onCheckFile(file, checked) {
        const pathsToImport = Object.assign({}, this.state.pathsToImport, {[file]: checked});
        this.setState({pathsToImport});
    }

    checkAll() {
        const pathsToImport = {};
        this.props.filelist.forEach((filepath) => (pathsToImport[filepath] = true));
        this.setState({pathsToImport});
    }

    importStatements() {
        const paths = Object.keys(this.state.pathsToImport)
            .filter((filepath) => this.state.pathsToImport[filepath])
            .map((filepath) => path.join(this.props.account.importFolder, filepath));
        this.setState({pathsToImport: {}});
        this.props.importStatement(this.props.account.id, paths);
    }

    render() {
        const rootPath = path.join('/import/browser/', (this.props.account.importFolder||""));

        return <div className='statements-to-import'>
            <h2>Statements to Import</h2>
            <div className='controls'>
                <IconButton onClick={this.checkAll} icon="check-double" />
                <IconButton onClick={this.importStatements} icon="file-import" />
            </div>        

            <FileList 
                onCheck={this.onCheckFile}
                checked={this.state.pathsToImport}
                contents={this.props.filelist}
                rootPath={rootPath} />
        </div>
    }
})