import React from "react";
import ReactDOM from "react-dom";
import {connect} from 'react-redux'
import MainLayout from '../layouts/main-layout'
import Account from '../../api/accounts/account'
import EditAccount from './edit-account'
import AccountSummary from './account-summary'

export default connect((purrStore) => ({
    accounts: purrStore.accounts
}), {})(
class Accounts extends React.Component {
    render() {
        return <MainLayout path="Accounts">
            <div className='accounts'>
                <h2>Accounts</h2>
                <EditAccount account={(new Account())}/>
                {
                    Object.keys(this.props.accounts).map((accountId) => 
                        <AccountSummary key={accountId} account={this.props.accounts[accountId]} />)
                }
            </div>
        </MainLayout>;
    }
})