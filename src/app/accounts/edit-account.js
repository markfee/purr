import React from "react";
import {connect} from 'react-redux';
import {get, save, edit} from '../api-services/api-actions';
import {TextInput, IconButton, ValuePicker} from "../core/components";
import path from 'path';

const defaults = require('../../api/defaults');

export default connect((purrStore) => ({
    browser: purrStore.browser
}), {get, save, edit})(
class EditAccount extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onImportFolderChange = this.onImportFolderChange.bind(this);
        this.close = this.close.bind(this);
        this.expand = this.expand.bind(this)
        this.save = this.save.bind(this)
        
        this.state = {
            account: this.props.account,
            new: !Boolean(this.props.account.id),
            expanded: Boolean(this.props.account.id)
        }
    }

    save() {
        this.props.save('accounts', this.state.account, this.props.account.id);
        this.close();
    }

    expand() {
        this.setState({expanded: true});
    }

    close() {
        if (this.state.new) {
            this.setState({
                expanded: false,
                account: Object.assign({}, this.props.account)
            });
        } else {
            this.props.edit('accounts', this.props.account.id, false);
        }
    }

    onChange(event) {
        const target = event.target;
        this.setState({
            account: Object.assign({}, this.state.account, {[event.target.name]: event.target.value})
        });
    };

    onImportFolderChange(folder) {
        this.setState({
            account: Object.assign({}, this.state.account, {importFolder: folder})
        });
    };
    
    componentDidMount() {
        this.props.get('browser', ".");
    }

    render() {
        if (!this.state.expanded) {
            return <div className='account' >
                <div className='values'>
                    <div className='controls'>
                        <IconButton onClick={this.expand} icon="plus" />
                    </div>
                </div>
            </div>;
        }
        const folderList = (this.props.browser.content || [])
            .map((folder) => {
                folder = path.join('/', folder);
                return {name: folder, id: folder}
            });
        return <div className='account' >
            {!this.props.new && <h2>{this.state.account.name}  </h2>}
            <div className='values'>
                <TextInput 
                    name='name'
                    label="Name"
                    value={this.state.account.name}
                    onChange={this.onChange} />
                <TextInput
                    name='sortCode'
                    label="Sort Code"
                    value={this.state.account.sortCode}
                    onChange={this.onChange} />
                <TextInput 
                    name='accountNumber'
                    label="Account Number" 
                    value={this.state.account.accountNumber} 
                    onChange={this.onChange} />
                <ValuePicker
                    value={this.state.account.importFolder}
                    values={folderList}
                    label="Import Folder" 
                    onChange={this.onImportFolderChange}/>

                <div className='controls'>
                    <IconButton onClick={this.close} icon="times-circle" />
                    <IconButton onClick={this.save} icon="save" />
                </div>
            </div>
        </div>;
    }
})