import React from "react";
import {connect} from 'react-redux';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import {ValuePair, IconButton, AmountValue} from "../core/components";
import EditAccount from './edit-account';
import {edit, destroy} from '../api-services/api-actions';

export default connect((purrStore) => ({
    accounts: purrStore.accounts
}), {edit, destroy})(
class AccountSummary extends React.Component {
    constructor(props) {
        super(props);
        this.destroy = () => this.props.destroy('accounts', this.props.account.id);
        this.edit = () => this.props.edit('accounts', this.props.account.id);
    }

    render() {
        const account = this.props.account;
        
        if (account.editing) {
            return <EditAccount account={this.props.account} />
        }

        return <div className='account' >
            <h2>
                {account.name} 
                <Link to={`/accounts/${account.id}`}>
                    <IconButton icon="arrow-circle-right" />
                </Link>
            </h2>
            <div className='values'>
                <ValuePair label="Sort Code" value={account.sortCode} />
                <ValuePair label="Account Number" value={account.accountNumber} />
                <ValuePair label="Import Folder" value={account.importFolder} />

                <AmountValue label="Balance">{account.balance}</AmountValue>

                <div className='controls'>
                    <IconButton onClick={this.edit} icon="edit" />
                    <IconButton onClick={this.destroy} icon="trash-alt" />
                </div>
            </div>
        </div>
    }
});