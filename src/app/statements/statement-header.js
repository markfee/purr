import React from "react";
import { AmountValue, MinDateValue } from '../core/components';

const StatementHeader = (props) => <React.Fragment>
    <MinDateValue dates={[props.statement.startDate, props.statement.endDate]} format="MMM-YYYY"/>
    <AmountValue>{props.statement.broughtForward}</AmountValue>
    <AmountValue>{props.statement.carriedForward}</AmountValue>
</React.Fragment>

export default StatementHeader;