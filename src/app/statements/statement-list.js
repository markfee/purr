import React from "react";
import StatementHeader from './statement-header';

const StatementList = (props) => <div className='statements'>
    <h2> Statements</h2>
    <div className='statement-list'>
        <div className='header'>
            <label>Month</label>
            <label>Brought Forward</label>
            <label>Carried Forward</label>
        </div>
        {props.statements.map((statement) =>
            <div key={statement.statementId} className='statement' >
                <StatementHeader statement={statement} />
            </div>)
        }
    </div>
</div>;

export default StatementList;