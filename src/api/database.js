const low = require('lowdb');
const lodashId = require('lodash-id');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('db.json');
const db = low(adapter);

const defaults = require('./defaults');
const examples = defaults.examples;

db._.mixin(lodashId);

db.defaults({
    accounts: [examples.account],
    statements: [examples.statement]
}).write();


module.exports = db;