const express = require('express');
const serveIndex = require('serve-index');
const CsvStatementParser = require('../statements/csv-statement-parser');

module.exports = (() => {
    const api = express.Router();
    const parser = new CsvStatementParser();
    api.get('/*', serveIndex(parser.rootPath));

    api.get('/*/:filename*.csv', (req, res) => {
        CsvStatementParser.parseFile(decodeURI(req.path)).then((statement) => res.send(statement));
    });

    return api;
})();