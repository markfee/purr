const moment = require('moment');

const exampleAmount = Object.freeze({
    currency: "GBP",
    value: 0
});

const exampleTransaction = Object.freeze({
    "date": moment(), 
    "description": "Example Transaction", 
    "amount": exampleAmount,
    "balance": exampleAmount
});

const exampleStatement = Object.freeze({
    id: "example_statement", 
    "startDate": moment(), 
    "endDate": moment(), 
    "reconciled": true,
    "broughtForward": exampleAmount, 
    "carriedForward": exampleAmount, 
    "transactions": [Object.assign({}, exampleTransaction)],
});

const exampleAccount = Object.freeze({
    id: "example_account", 
    name: "Example Account", 
    sortCode: '00-00-00', 
    accountNumber: '12345678',
    statements: ["example_statement"],
    transactions: [Object.assign({}, exampleTransaction)],
});

module.exports = {
    examples: {
        amount: exampleAmount,
        account: exampleAccount,
        transaction: exampleTransaction,
        statement: exampleStatement,
    }
}