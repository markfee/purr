/* PURR API routes index.js */
const express = require('express');

module.exports = (() => {
    const api = express.Router();

    api.get('/', (req, res) => {
        res.send({api: "This is the API Index"})
    });
    // API routes are served by this node server
    api.use('/accounts', require('./accounts'));
    api.use('/browser', require('./import'));
    api.use('/statements', require('./statements'));
    return api;
})();