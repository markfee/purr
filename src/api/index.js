const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

// API routes are served by this node server
app.use('/api', express.json(), require('./api'));

// Static files are served from the dist folder
app.use(express.static('./dist'))

// All other routes are served by the front end application
const indexFilePath = path.join(__dirname, '/../../dist/index.html');
app.use((request, response) => response.sendFile(indexFilePath));

app.listen(port, () => 
    console.log(`The Purr server is listening on port ${port}`)
);
