const express = require('express');
const db = require('./database');

module.exports = (object) => {
    const api = express.Router();

    api.get('/', (req, res) => {
        res.send(db.get(object.dbKey()).value())
    });

    api.get('/:id', (req, res) => {
        res.send(
            db.get(object.dbKey())
                .find({id: req.params["id"]})
                .value()
        );
    });

    api.delete('/:id', (req, res) => {
        db.get(object.dbKey())
            .remove({id: req.params["id"]})
            .write();
        res.status(200).send();
    });    

    api.post('/', (req, res) => {
        res.status(200).send(
            db.get(object.dbKey())
                .insert((new object(req.body)))
                .write()
        );
    });

    api.post('/:id', (req, res) => {
        res.status(200).send(
            db.get(object.dbKey()).find({id: req.params["id"]})
                .assign(req.body)
                .assign({editing: false})
                .write());
    });
    return api;
};