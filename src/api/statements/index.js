const makeApi = require('../make-api');
const Statement = require('./statement');

const statementsApi = makeApi(Statement);

module.exports = (() => statementsApi)();