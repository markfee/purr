const path = require('path');
const csv = require('csvtojson')
const moment = require('moment')
const Amount = require('../amounts/amount');
const Statement = require('../statements/statement');
const fs = require('fs-extra');
const config = require('../config.json');

module.exports = class CsvStatementParser {
    constructor(filepath, accountId) {
        this.rootPath = config.rootPath;
        this.accountId = accountId;
        this.filepath = filepath;
    }

    parse() {
        const fullpath = path.join(this.rootPath, this.filepath);
        return csv().fromFile(fullpath)
            .then((jsonObj) =>  {
                const transactions = this.transformFirstDirectCsv(jsonObj);
                return new Statement({ accountId: this.accountId, header: {filepath: this.filepath}, transactions });
            })
    }

    markAsImported() {
        const fullpath = path.join(this.rootPath, this.filepath);
        const newPath  = path.join(path.dirname(fullpath), 'imported');
        const newLocation = path.join(newPath, path.basename(fullpath));
        fs.mkdirp(newPath).then(() => {
            fs.rename(fullpath, newLocation).then(() => {
            });
        }).catch((err) => {
            console.log("Error Creating Path", newPath, "\n", err);
        });
    }

    // TODO handle others
    transformFirstDirectCsv(transactions) {
        return transactions.map((transaction) => ({
            date: moment(transaction.Date, "DD/MM/YYYY"), 
            description: transaction.Description, 
            amount: Amount.parseFloat(transaction.Amount),
            balance: Amount.parseFloat(transaction.Balance)
        }));
    }

    static parseFile(filepath, accountId) {
        return new CsvStatementParser(filepath, accountId).parse();
    }
}