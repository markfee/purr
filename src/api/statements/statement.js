const Amount = require('../amounts/amount');

class Statement {
    constructor(statement) {
        const transactions = statement.transactions || [];
        const firstDate = transactions[0] && transactions[0].date;
        const lastDate = transactions[transactions.length-1] && transactions[transactions.length-1].date;
        const isAscending = Boolean(firstDate < lastDate);

        const firstTransaction = isAscending ? transactions[0] : transactions[transactions.length-1];
        const lastTransaction = isAscending ? transactions[transactions.length-1] : transactions[0];

        Object.assign(this, {
            transactions: []
        }, statement);

        const broughtForward = (new Amount(firstTransaction && firstTransaction.balance)).subtract(new Amount(firstTransaction.amount));

        this.header = Object.assign({
            isAscending,
            startDate: firstTransaction && firstTransaction.date,
            endDate: lastTransaction && lastTransaction.date,
            broughtForward,
            carriedForward: lastTransaction && lastTransaction.balance || new Amount(),
            reconciled: false
        }, this.header);
    }

    static dbKey() {
        return "statements";
    }

    getHeader() {
        return Object.assign({statementId: this.id}, this.header);
    }
}

module.exports = Statement;