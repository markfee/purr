const Amount = require('../amounts/amount');
const moment = require('moment');
class Account {
    /*
        newValues is optional allowing us to copy an account with mutated values
        without mutating the original
    */
    constructor(account, newValues) {
        Object.assign(this, {
            name: 'Account',
            sortCode: '00-00-00', 
            accountNumber: '00000000',
            balance: new Amount(0),
            filepath: "",
            statements: [],
            transactions: []
        }, account, newValues);
    }

    static dbKey() {
        return "accounts";
    }

    addStatement(statement) {
        const statements = [statement.getHeader()]
            .concat(this.statements)
            .sort((a, b) => moment(b.endDate).diff(moment(a.endDate)));
        // TODO Handle recent transactions with respect to balance
        const balance = statements[0].carriedForward;
        return new Account(this, {statements, balance});
    }
}

module.exports = Account;