const makeApi = require('../make-api');
const Account = require('./account');
const Statement = require('../statements/statement');
const importsApi = require('../import');
const accountsApi = makeApi(Account);
const CsvStatementParser = require('../statements/csv-statement-parser');
const db = require('../database');
const path = require('path');

const getAccount = (req, res) => {
    const accountId = req.params["id"];
    // 1) Get The Account
    let account = db.get("accounts").getById(accountId);
    if (account.value() === undefined ){
        res.status(404).send();
        return;
    }
    account = new Account(account.value());
    return {accountId, account};
}
/*
    Import a statement from a file.
    Creates a Statement Containing Transactions that will be read in from the server file
    whose path is passed in as req.body.filePath
*/
accountsApi.post('/:id/statements', (req, res) => {
    /*TODO:
        Make sure a statement isn't imported twice
        Import an entire folder
    */

/*    const accountId = req.params["id"];
    // 1) Get The Account
    let account = db.get("accounts").getById(accountId);
    if (account.value() === undefined ){
        res.status(404).send();
        return;
    }
    account = new Account(account.value());*/
    const {accountId, account} = getAccount(req, res);
    if (!accountId) {
        return;
    }
    const filepaths = [].concat(req.body.filepaths);

    const promises = filepaths.map((filepath) => new Promise((resolve, reject) => {
        // Ignore files without csv extension
        // TODO accept other extensions/formats and handle errors gracefully
        if (path.extname(filepath).toLowerCase() !== '.csv') {
            console.log("Skipping file withouth csv extension", filepath);
            resolve();
            return;
        }
        // Parse The Statement
        const parser = new CsvStatementParser(filepath, accountId);
        parser.parse().then((statement) => {
            // Write The Statement
            const newStatement = new Statement(db.get("statements")
                .insert(statement)
                .write());

            //  Append The Statement ID to the account
            account = account.addStatement(newStatement);
            // Move the imported file to the imported folder
            parser.markAsImported();
            resolve();
        });
    }));
    Promise.all(promises).then(() => {
        // Write the new statement and the new balance back to the account db
        const newRecord = db.get("accounts").find({id: accountId})
            .assign({
                statements: account.statements,
                balance: account.balance
            })
            .write();
        // return the updated account
        res.status(200).send(account);
    })
}); 

/*
    Should Get All Statements for an account
    Each Statement should have its header conents removed (except for the original filePath)
    The Statement body should then be passed to a new Statement() which will recreate the header
    The Accounts statements should be replaced with the new rebuilt statement
    The Individual Statements should also have their headers replaced with the new headers

*/
accountsApi.get('/:id/rebuild-statements', (req, res) => {
    const {accountId, account} = getAccount(req, res);
    if (!accountId) {
        return;
    }
    const statements = (account.statements || []).map((statement) => {
            const newStatement = db.get(Statement.dbKey())
                .find({id: statement.statementId})
                .value();
            newStatement.header = newStatement.header ? {
                filepath: newStatement.header.filepath
            } : {};
            return new Statement(newStatement);
    });
    res.status(200).send(statements);
});

module.exports = (() => accountsApi)();