class Amount {
    constructor(value = 0, currency = 'GBP') {
        this.value = typeof value === "object" ? value.value : value;
        this.currency = typeof value === "object" && value.currency || currency;
    }
    
    static parseFloat(strAmount, currency) {
        return new Amount(Math.round(strAmount * 100), currency);
    }

    fromJSON(amount) {
        return new Amount(amount.value, amount.currency);
    }

    toString() {
        return new Intl.NumberFormat('en-UK', {
            style: 'currency',
            currency: this.currency,
            currencyDisplay: 'symbol',
        }).format(this.value/100);
    }

    valueOf() {
        return this.value;
    }

    add(amount) {
        return new Amount(this.value + (amount.value || 0));
    }

    subtract(amount) {
        return new Amount(this.value - (amount.value || 0));
    }
}
module.exports = Amount;