export const {
    faSave,
    faEdit,
    faTrashAlt,
    faFileImport,
    faTimesCircle,
    faArrowCircleRight,
    faPlus,
    faCheckDouble
} = require('@fortawesome/free-solid-svg-icons');