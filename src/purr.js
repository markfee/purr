import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux'
import purrStore from './app/api-services/purr-store';
import Routes from './app/routes';
import './styles/';

ReactDOM.render(
    <Provider store={purrStore}>
        <Routes/>
    </Provider>, 
    document.getElementById("purr")
);